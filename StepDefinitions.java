package cucumberintro;

//import cucumberintro.ohrm.utils.TestTemplate;
import io.cucumber.java.en.*;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import ohrm.tests.TestTemplate;
//import ohrm.utils.TestTemplate;
//import org.junit.After;
//import org.junit.Before;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.junit.jupiter.api.Disabled;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static ohrm.utils.TestCommons.*;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ohrm.utils.TestCommons.BASE_URL;
import static ohrm.utils.TestCommons.TITLE_HOME;
import static org.junit.Assert.*;

public class StepDefinitions extends TestTemplate {

    //private String incorrectUser;
    //private String invalidCredentials;
    //private String incorrectPassword;

    public StepDefinitions() {

    }

    @Before
    public void setup() {
        set();
    } // sets the requirements that are under - public void set - under "TestTemplate"

    @After
    public void tearDown() {
        driver.quit();
    } // closes all the opened windows

    @Given("I am on the application login page")
    public void openHomePage() {
        driver.get(BASE_URL);
        driver.manage().window().maximize();
    }

    @When("I enter {string} in the \"Username\" field")
    public void insertUser(String user) {
        driver.findElement(By.cssSelector("#divUsername > .form-hint")).click();
        driver.findElement(By.id("txtUsername")).sendKeys(user);
    }

    @And("I enter {string} in the \"Password\" field")
    public void insertPassword(String password) {
        driver.findElement(By.id("txtPassword")).click();
        driver.findElement(By.id("txtPassword")).sendKeys(password);
    }

    @And("I click on the \"Login\" button")
    public void submitLoginForm() {

        driver.findElement(By.id("btnLogin")).click();
    }

    @Then("I am on homepage")
    public void checkHomePage() {
        wait.until(ExpectedConditions.titleIs(TITLE_HOME));
        assertTrue(driver.getTitle().equals(TITLE_HOME));
    }

    //@Then("the error message {string} is displayed")
    //public void theErrorMessageIsDisplayed(String arg0) {
        //driver.findElement(By.id("Invalid credentials"));
    //}

    //@But("home page is not visible")
    //public void homePageIsNotVisible() {
        //driver.findElement(By.id("Invalid credentials"));
        //assertTrue(driver.getCurrentUrl().equals(BASE_URL));
    //}

    //@And("I enter {string} in the {string} Field")
    //public void iEnterInTheField(String arg0, String arg1) {
        //driver.findElement(By.id("txtPassword")).click();
        //driver.findElement(By.id("txtPassword")).sendKeys(incorrectPassword);
    //}


}