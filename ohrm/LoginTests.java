package ohrm.tests;

import ohrm.utils.Steps;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.*;

import static ohrm.utils.TestCommons.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LoginTests extends TestTemplate {
    public LoginTests() {
    }

    @BeforeAll
    public void setUp() {
        this.set();
    }

    @AfterAll
    public void tearDown() {
        driver.quit();
    }

    @AfterEach
    public void closeWindow() {
        log.info("Close window");
        driver.close();
    }

    @Test
    @DisplayName("Login is successful with correct User and Password")
    public void loginSuccessful() {
        log.info("Login");
        Steps.login(driver, wait, js, USER, PASSWORD);
        assertTrue(driver.getTitle().equals(TITLE_HOME));

    }

    @Test
    @DisplayName("Login fails with incorrect User and correct Password")
    public void loginFailsIncorrectUser() {
        Steps.login(driver, wait, js, INCORRECTUSER, PASSWORD);
        assertTrue(driver.getCurrentUrl().equals(BASE_URL));

    }

    @Test
    @DisplayName("Login fails with correct User and incorrect Password")
    public void loginFailsIncorrectPassword() {
        Steps.login(driver, wait, js, USER, INCORRECTPASSWORD);
        assertTrue(driver.getCurrentUrl().equals(BASE_URL));

    }


}
