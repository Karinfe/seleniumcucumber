Feature: Login
  As a user that is not logged in, I want to login with my username "opensourcecms" and password "opensourcecms" to access my account.

  Scenario: Test1: Login is successful with correct User and Password
    Given I am on login page
    When I enter "opensourcecms" in the "username" field
    And I enter "opensourcecms" in the "password" field
    And I click the Login button
    Then I am on home page

  Scenario: Test2: Login is unsuccessful with incorrect User and correct Password
    Given I am on login page
    When I enter "WrongUsername" in the "username" field
    And I enter "opensourcecms" in the "password" field
    And I click the Login button
    Then I am on login page
    And an error message "Invalid credentials" is displayed
    But homepage is not displayed

  Scenario: Test3: Login is unsuccessful with correct User and incorrect Password
    Given I am on login page
    When I enter "opensourcecms" in the "username" field
    And I enter "WrongPassword" in the "password" field
    And I click the Login button
    Then I am on login page
    And an error message "Invalid credentials" is displayed
    But homepage is not displayed
