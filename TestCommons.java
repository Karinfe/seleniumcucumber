package ohrm.utils;

public class TestCommons {
    //URLs
    public static final String BASE_URL = "https://s2.demo.opensourcecms.com/orangehrm/symfony/web/index.php/auth/login/";

    //Page title
    public static final String TITLE_HOME = "OrangeHRM";

    //Login data
    public static final String USER = "opensourcecms";
    public static final String PASSWORD = "opensourcecms";
    public static final String INCORRECTUSER = "incorrectUser";
    public static final String INCORRECTPASSWORD = "incorrectPassword";


    //Messages
    public static final String SKILL_DUPLICATED = "Already exists";
    public static final String SKILL_SAVE_SUCCESS = "Successfully Saved";
    public static final String SKILL_SAVE_FAILED = "Successfully Deleted";

    public TestCommons() {

    }
}
